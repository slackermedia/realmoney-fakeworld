# Real Money in a Fake World

A short screenplay about...well, it's in the title. This screenplay
has been made into a short film at least 24 times at Pittsburgh
Filmmakers Institute.

Written in Emacs using screenplay-mode, licensed [Creative Commons
BY-SA](https://creativecommons.org/licenses/by-sa/4.0/).


## How do I print?

As long as you open this screenplay in a proper text editor like [GNU
Emacs](http://www.gnu.org/software/emacs/), you should be able to
print with enough accuraty to maintain the one minute per page rule.

Another way is to produce a PDF with the ``pr`` command and
``text2pdf`` commands:

    $ pr title.page realMoneyInAFakeWorld.scp | text2pdf > print.pdf


## Can I use this screenplay?

Yes, you can film this screenplay, you can adapt the story, you can
steal ideas, you can use this in film school, you can animate it, you
can even sell it. You can do pretty much anything you want, as long as
you give credit, and permit others to do exactly as you have done.

